declare class ProgressMovv extends HTMLElement {
    constructor();
    get winScroll(): number;
    get preHeight(): number;
    get scrolled(): number;
}
