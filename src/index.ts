class ProgressMovv extends HTMLElement {
	constructor() {
		super()

		// shadowDOM Root
		const shadow = this.attachShadow({ mode: 'open' })

		// 프로그레스 바 설정
		const progressBar = document.createElement('div')
		progressBar.setAttribute('class', 'progressBar')

		// 빈 프로그레스바 설정
		const nullBar = document.createElement('div')
		nullBar.setAttribute('class', 'nullBar')

		// 아이콘 설정
		const icon = document.createElement('span')
		icon.setAttribute('class', 'icon')

		// 아이콘에 넣을 이미지 설정
		const img = document.createElement('img')
		img.setAttribute('class', 'img')
		img.src = 'img/ic_car_control.png'

		// 이미지 부착
		icon.appendChild(img)

		progressBar.appendChild(nullBar)

		const style = document.createElement('style')

		style.textContent = `
        .progressBar {
            background-color: #38B0DE;
            border-radius: 4px;
            box-shadow: inset 0 0.5em 0.5em rgba(0,0,0,0.05);
            height: 5px;
            margin: 1rem 0 1rem 0;
            overflow: hidden;
            position: relative;
            transform: translateZ(0);
            width: 100%;
        }
        .nullBar {
            background-color: #38B0DE;
            box-shadow: inset 0 0.5em 0.5em rgba(94, 49, 49, 0.05);
            bottom: 0;
            left: 0;
            position: absolute;
            right: 0;
            top: 0;
        }
        .icon {
            display : none;
            bottom: 0;
            left: 0;
            position: relative;
            right: 0;
            top: 0;
        }
        `

		// showDOM에 자식 노드들 부착
		shadow.appendChild(progressBar)
		shadow.appendChild(style)
		shadow.appendChild(icon)

		// 윈도우가 스크롤 이벤트 발생 시 프로그레스 바 실행
		window.addEventListener('scroll', e => {
			progressBar.style.width = this.scrolled + '%'
			icon.style.display = 'block'
			icon.style.transform = `translateX(${this.scrolled}%)`
			console.log(this.winScroll)
		})
	}

	// 스크롤 시 페이지 상단의 위치값을 반환
	get winScroll() {
		return document.body.scrollTop || document.documentElement.scrollTop
	}

	// 스크롤 시키지 않을 때 전체 높이에서 클라이언트가 스크롤 시켰을 때의 높이 차
	get preHeight() {
		return document.documentElement.scrollHeight - document.documentElement.clientHeight
	}

	// 상단의 위치 값에 높이 차를 나눈 퍼센트 값 반환
	get scrolled() {
		return (this.winScroll / this.preHeight) * 100
	}
}

customElements.define('progress-bar', ProgressMovv)
